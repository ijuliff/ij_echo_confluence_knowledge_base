# Amazon Echo Knowledge Base for Confluence

## Goal
Confluence can be used as knowledge base for storing how to articles.
The goal is use these articles to provide a voice interactive user friendly system to query for information via Amazon Echo aka Alexa!


## Sample Interaction Phrases

##### Confluence Search

Based on users query, information is retrieved using confluence search functionality

If the search results contain a single knowledge base article then alexa reads information from the confluence page to the user

```javascript
User : Alexa!  Ask <InvocationWord>, "How to use Create HR Request ?"
Alexa: <Explains how to create hr request>
```

If confluence search returns multiple search results, then alexa presents the top 5 results requesting the user to make a selection.
Based on the selection, alexa reads information to the user

```javascript
User : Alexa!  Ask <InvocationWord>, "How to connect to Guest Wifi?"
Alexa: I found the following results from wiki . Please choose from the following .
        1 . how to connect to guest wifi
        2 . how to connect to guest VPN
User : Number 1
Alexa: <Explains user on how to connect to wifi>
```


## Setup Alexa Skill for Echo


This code uses [alexa-app](https://www.npmjs.com/package/alexa-app) as a
framework to build an Alexa Skill.

To deploy the skill on a staging Echo, we need two services:

1. Create a lambda function via https://console.aws.amazon.com/lambda
2. Create an Alexa skill via https://developer.amazon.com/



#### Confluence url and space information

The alexa skill return best matched results and answers from confluence knowledge base. In order for the search to work successfully we need to enter valid confluence url and space key to search in

Config.json
```javascript
{
    "ConfluenceBaseURL": "https://addteq.atlassian.net/wiki",
    "ConfluenceSpace" : "IHD"
}
```

#### Create Code ZIP by compress the following files into a zip:

    index.js
    confluence.js
    package.json
    nodemodules
    config.json

#### Setup Lambda Function

Log into AWS Lambda. https://console.aws.amazon.com/lambda/
You must be logged in on US-East (N. Virginia) in order to access the Alexa Service from AWS Lambda.  To switch your location, simply click the location displayed next to your name in the top right of the AWS Console.

Click Create a Lambda Function.  On the screen about selecting a blueprint, click skip.
If you do not find create a lamda function , click on getting started.

Next associate the triggers required for alexa function.  Add the Alexa Skill Kit trigger
![Create Lambda Function](images/alexa-confluence.png "Create Lambda Function")


Now we can configure the Lambda function that will host our skill service by filling out the empty fields here:

1. For Name, enter confluenceSearch
2. For Runtime, select Node.js
3. For Role, select `lambda_basic_execution`

If no role is present , Choose `create a custom role`. In the new tab that opens , have default values as `lambda_basic_execution` and click on allow.

Finally, select upload a ZIP file under Lambda function code and choose the archive file you created.  You should wind up with a completed form that looks like this:

![Create Lambda Function](images/FunctionSetup.png "Create Lambda Function")

Make sure to copy the ARN at the top right of the page. This is the Amazon Resource Name, and the skill interface we™re going to configure next needs that value in order to know where to send events.  The ARN will look something like `arn:aws:lambda:us-east-1:333333289684:function:myFunction`.

![ARN Code](images/arncode.png "Arn code example")

We can test that the Lambda function works correctly by sending it a test event within the Lambda interface.  Click the blue Test button at the right of the screen, and under Sample Event Template select Alexa Start Session.  Click Save and Test.

At this point, the skill service we have is live.  Now we can move on to getting the skill interface set up.


### Create Alexa Skill

The AWS Lambda instance is live and we™ve got its ARN value copied, so now we can start configuring the skill interface to do its work: resolving a user™s spoken utterances to events our service can process.

Go to the [Alexa Skills Kit Portal](https://developer.amazon.com/edw/home.html#/skills/list) and click Add a New Skill.  Enter Knowledge base  for Name. Invocation name will be used to wake up our skill in alexa.  Some examples can be jarvis , titan , athena , bob ,etc...

The value for Invocation Name is what a user will say to Alexa to trigger the skill.  Paste the ARN you copied into the Endpoint box (Make sure the Lambda ARN button is selected).  The Skill Information page should look like this:

![Create Alexa Skill](images/alexa-skill-create.png "Create Alexa Skill")

#### Setting up the Interaction Model

We™ve already gotten the values needed for the Interaction Model page; it's mentioned below.

Copy the values from the Intent Schema and Sample Utterances fields from below and paste them into the respective fields on the Interaction Model page

##### Intent Schema
```javascript
{
  "intents": [
    {
      "intent": "confluenceSearch",
      "slots": [
        {
          "name": "RTEXT",
          "type": "LITERAL"
        }
      ]
    },
    {
      "intent": "searchDetails",
      "slots": [
        {
          "name": "UInput",
          "type": "AMAZON.NUMBER"
        }
      ]
    }
  ]
}
```

##### Sample Utterances
The utterances recognized by Alexa need to be defined for each Intent.  Here are a list of utterance combinations created for the above Intents; you can add more phrases for each Intent.

```javascript
confluenceSearch	who {do something|RTEXT}
confluenceSearch	what {do something|RTEXT}
confluenceSearch	how {do something|RTEXT}
confluenceSearch	where {do something|RTEXT}
confluenceSearch	who is {do something|RTEXT}
confluenceSearch	what is {do something|RTEXT}
confluenceSearch	how is {do something|RTEXT}
confluenceSearch	where is {do something|RTEXT}
confluenceSearch	who to {do something|RTEXT}
confluenceSearch	what to {do something|RTEXT}
confluenceSearch	how to {do something|RTEXT}
confluenceSearch	where to {do something|RTEXT}
confluenceSearch	who do {do something|RTEXT}
confluenceSearch	what do {do something|RTEXT}
confluenceSearch	how do {do something|RTEXT}
confluenceSearch	where do {do something|RTEXT}
confluenceSearch	who I {do something|RTEXT}
confluenceSearch	what I {do something|RTEXT}
confluenceSearch	how I {do something|RTEXT}
confluenceSearch	where I {do something|RTEXT}
confluenceSearch	who get {do something|RTEXT}
confluenceSearch	what get {do something|RTEXT}
confluenceSearch	how get {do something|RTEXT}
confluenceSearch	where get {do something|RTEXT}
confluenceSearch	who is get {do something|RTEXT}
confluenceSearch	what is get {do something|RTEXT}
confluenceSearch	how is get {do something|RTEXT}
confluenceSearch	where is get {do something|RTEXT}
confluenceSearch	who to get {do something|RTEXT}
confluenceSearch	what to get {do something|RTEXT}
confluenceSearch	how to get {do something|RTEXT}
confluenceSearch	where to get {do something|RTEXT}
confluenceSearch	who do get {do something|RTEXT}
confluenceSearch	what do get {do something|RTEXT}
confluenceSearch	how do get {do something|RTEXT}
confluenceSearch	where do get {do something|RTEXT}
confluenceSearch	who I get {do something|RTEXT}
confluenceSearch	what I get {do something|RTEXT}
confluenceSearch	how I get {do something|RTEXT}
confluenceSearch	where I get {do something|RTEXT}
confluenceSearch	who find {do something|RTEXT}
confluenceSearch	what find {do something|RTEXT}
confluenceSearch	how find {do something|RTEXT}
confluenceSearch	where find {do something|RTEXT}
confluenceSearch	who is find {do something|RTEXT}
confluenceSearch	what is find {do something|RTEXT}
confluenceSearch	how is find {do something|RTEXT}
confluenceSearch	where is find {do something|RTEXT}
confluenceSearch	who to find {do something|RTEXT}
confluenceSearch	what to find {do something|RTEXT}
confluenceSearch	how to find {do something|RTEXT}
confluenceSearch	where to find {do something|RTEXT}
confluenceSearch	who do find {do something|RTEXT}
confluenceSearch	what do find {do something|RTEXT}
confluenceSearch	how do find {do something|RTEXT}
confluenceSearch	where do find {do something|RTEXT}
confluenceSearch	who I find {do something|RTEXT}
confluenceSearch	what I find {do something|RTEXT}
confluenceSearch	how I find {do something|RTEXT}
confluenceSearch	where I find {do something|RTEXT}
confluenceSearch	who search {do something|RTEXT}
confluenceSearch	what search {do something|RTEXT}
confluenceSearch	how search {do something|RTEXT}
confluenceSearch	where search {do something|RTEXT}
confluenceSearch	who is search {do something|RTEXT}
confluenceSearch	what is search {do something|RTEXT}
confluenceSearch	how is search {do something|RTEXT}
confluenceSearch	where is search {do something|RTEXT}
confluenceSearch	who to search {do something|RTEXT}
confluenceSearch	what to search {do something|RTEXT}
confluenceSearch	how to search {do something|RTEXT}
confluenceSearch	where to search {do something|RTEXT}
confluenceSearch	who do search {do something|RTEXT}
confluenceSearch	what do search {do something|RTEXT}
confluenceSearch	how do search {do something|RTEXT}
confluenceSearch	where do search {do something|RTEXT}
confluenceSearch	who I search {do something|RTEXT}
confluenceSearch	what I search {do something|RTEXT}
confluenceSearch	how I search {do something|RTEXT}
confluenceSearch	where I search {do something|RTEXT}
confluenceSearch	who talk {do something|RTEXT}
confluenceSearch	what talk {do something|RTEXT}
confluenceSearch	how talk {do something|RTEXT}
confluenceSearch	where talk {do something|RTEXT}
confluenceSearch	who is talk {do something|RTEXT}
confluenceSearch	what is talk {do something|RTEXT}
confluenceSearch	how is talk {do something|RTEXT}
confluenceSearch	where is talk {do something|RTEXT}
confluenceSearch	who to talk {do something|RTEXT}
confluenceSearch	what to talk {do something|RTEXT}
confluenceSearch	how to talk {do something|RTEXT}
confluenceSearch	where to talk {do something|RTEXT}
confluenceSearch	who do talk {do something|RTEXT}
confluenceSearch	what do talk {do something|RTEXT}
confluenceSearch	how do talk {do something|RTEXT}
confluenceSearch	where do talk {do something|RTEXT}
confluenceSearch	who I talk {do something|RTEXT}
confluenceSearch	what I talk {do something|RTEXT}
confluenceSearch	how I talk {do something|RTEXT}
confluenceSearch	where I talk {do something|RTEXT}
confluenceSearch	who is {do something|RTEXT}
confluenceSearch	what is {do something|RTEXT}
confluenceSearch	how is {do something|RTEXT}
confluenceSearch	where is {do something|RTEXT}
confluenceSearch	who is is {do something|RTEXT}
confluenceSearch	what is is {do something|RTEXT}
confluenceSearch	how is is {do something|RTEXT}
confluenceSearch	where is is {do something|RTEXT}
confluenceSearch	who to is {do something|RTEXT}
confluenceSearch	what to is {do something|RTEXT}
confluenceSearch	how to is {do something|RTEXT}
confluenceSearch	where to is {do something|RTEXT}
confluenceSearch	who do is {do something|RTEXT}
confluenceSearch	what do is {do something|RTEXT}
confluenceSearch	how do is {do something|RTEXT}
confluenceSearch	where do is {do something|RTEXT}
confluenceSearch	who I is {do something|RTEXT}
confluenceSearch	what I is {do something|RTEXT}
confluenceSearch	how I is {do something|RTEXT}
confluenceSearch	where I is {do something|RTEXT}
confluenceSearch	who get is {do something|RTEXT}
confluenceSearch	what get is {do something|RTEXT}
confluenceSearch	how get is {do something|RTEXT}
confluenceSearch	where get is {do something|RTEXT}
confluenceSearch	who is get is {do something|RTEXT}
confluenceSearch	what is get is {do something|RTEXT}
confluenceSearch	how is get is {do something|RTEXT}
confluenceSearch	where is get is {do something|RTEXT}
confluenceSearch	who to get is {do something|RTEXT}
confluenceSearch	what to get is {do something|RTEXT}
confluenceSearch	how to get is {do something|RTEXT}
confluenceSearch	where to get is {do something|RTEXT}
confluenceSearch	who do get is {do something|RTEXT}
confluenceSearch	what do get is {do something|RTEXT}
confluenceSearch	how do get is {do something|RTEXT}
confluenceSearch	where do get is {do something|RTEXT}
confluenceSearch	who I get is {do something|RTEXT}
confluenceSearch	what I get is {do something|RTEXT}
confluenceSearch	how I get is {do something|RTEXT}
confluenceSearch	where I get is {do something|RTEXT}
confluenceSearch	who find is {do something|RTEXT}
confluenceSearch	what find is {do something|RTEXT}
confluenceSearch	how find is {do something|RTEXT}
confluenceSearch	where find is {do something|RTEXT}
confluenceSearch	who is find is {do something|RTEXT}
confluenceSearch	what is find is {do something|RTEXT}
confluenceSearch	how is find is {do something|RTEXT}
confluenceSearch	where is find is {do something|RTEXT}
confluenceSearch	who to find is {do something|RTEXT}
confluenceSearch	what to find is {do something|RTEXT}
confluenceSearch	how to find is {do something|RTEXT}
confluenceSearch	where to find is {do something|RTEXT}
confluenceSearch	who do find is {do something|RTEXT}
confluenceSearch	what do find is {do something|RTEXT}
confluenceSearch	how do find is {do something|RTEXT}
confluenceSearch	where do find is {do something|RTEXT}
confluenceSearch	who I find is {do something|RTEXT}
confluenceSearch	what I find is {do something|RTEXT}
confluenceSearch	how I find is {do something|RTEXT}
confluenceSearch	where I find is {do something|RTEXT}
confluenceSearch	who search is {do something|RTEXT}
confluenceSearch	what search is {do something|RTEXT}
confluenceSearch	how search is {do something|RTEXT}
confluenceSearch	where search is {do something|RTEXT}
confluenceSearch	who is search is {do something|RTEXT}
confluenceSearch	what is search is {do something|RTEXT}
confluenceSearch	how is search is {do something|RTEXT}
confluenceSearch	where is search is {do something|RTEXT}
confluenceSearch	who to search is {do something|RTEXT}
confluenceSearch	what to search is {do something|RTEXT}
confluenceSearch	how to search is {do something|RTEXT}
confluenceSearch	where to search is {do something|RTEXT}
confluenceSearch	who do search is {do something|RTEXT}
confluenceSearch	what do search is {do something|RTEXT}
confluenceSearch	how do search is {do something|RTEXT}
confluenceSearch	where do search is {do something|RTEXT}
confluenceSearch	who I search is {do something|RTEXT}
confluenceSearch	what I search is {do something|RTEXT}
confluenceSearch	how I search is {do something|RTEXT}
confluenceSearch	where I search is {do something|RTEXT}
confluenceSearch	who talk is {do something|RTEXT}
confluenceSearch	what talk is {do something|RTEXT}
confluenceSearch	how talk is {do something|RTEXT}
confluenceSearch	where talk is {do something|RTEXT}
confluenceSearch	who is talk is {do something|RTEXT}
confluenceSearch	what is talk is {do something|RTEXT}
confluenceSearch	how is talk is {do something|RTEXT}
confluenceSearch	where is talk is {do something|RTEXT}
confluenceSearch	who to talk is {do something|RTEXT}
confluenceSearch	what to talk is {do something|RTEXT}
confluenceSearch	how to talk is {do something|RTEXT}
confluenceSearch	where to talk is {do something|RTEXT}
confluenceSearch	who do talk is {do something|RTEXT}
confluenceSearch	what do talk is {do something|RTEXT}
confluenceSearch	how do talk is {do something|RTEXT}
confluenceSearch	where do talk is {do something|RTEXT}
confluenceSearch	who I talk is {do something|RTEXT}
confluenceSearch	what I talk is {do something|RTEXT}
confluenceSearch	how I talk is {do something|RTEXT}
confluenceSearch	where I talk is {do something|RTEXT}
confluenceSearch	who to {do something|RTEXT}
confluenceSearch	what to {do something|RTEXT}
confluenceSearch	how to {do something|RTEXT}
confluenceSearch	where to {do something|RTEXT}
confluenceSearch	who is to {do something|RTEXT}
confluenceSearch	what is to {do something|RTEXT}
confluenceSearch	how is to {do something|RTEXT}
confluenceSearch	where is to {do something|RTEXT}
confluenceSearch	who to to {do something|RTEXT}
confluenceSearch	what to to {do something|RTEXT}
confluenceSearch	how to to {do something|RTEXT}
confluenceSearch	where to to {do something|RTEXT}
confluenceSearch	who do to {do something|RTEXT}
confluenceSearch	what do to {do something|RTEXT}
confluenceSearch	how do to {do something|RTEXT}
confluenceSearch	where do to {do something|RTEXT}
confluenceSearch	who I to {do something|RTEXT}
confluenceSearch	what I to {do something|RTEXT}
confluenceSearch	how I to {do something|RTEXT}
confluenceSearch	where I to {do something|RTEXT}
confluenceSearch	who get to {do something|RTEXT}
confluenceSearch	what get to {do something|RTEXT}
confluenceSearch	how get to {do something|RTEXT}
confluenceSearch	where get to {do something|RTEXT}
confluenceSearch	who is get to {do something|RTEXT}
confluenceSearch	what is get to {do something|RTEXT}
confluenceSearch	how is get to {do something|RTEXT}
confluenceSearch	where is get to {do something|RTEXT}
confluenceSearch	who to get to {do something|RTEXT}
confluenceSearch	what to get to {do something|RTEXT}
confluenceSearch	how to get to {do something|RTEXT}
confluenceSearch	where to get to {do something|RTEXT}
confluenceSearch	who do get to {do something|RTEXT}
confluenceSearch	what do get to {do something|RTEXT}
confluenceSearch	how do get to {do something|RTEXT}
confluenceSearch	where do get to {do something|RTEXT}
confluenceSearch	who I get to {do something|RTEXT}
confluenceSearch	what I get to {do something|RTEXT}
confluenceSearch	how I get to {do something|RTEXT}
confluenceSearch	where I get to {do something|RTEXT}
confluenceSearch	who find to {do something|RTEXT}
confluenceSearch	what find to {do something|RTEXT}
confluenceSearch	how find to {do something|RTEXT}
confluenceSearch	where find to {do something|RTEXT}
confluenceSearch	who is find to {do something|RTEXT}
confluenceSearch	what is find to {do something|RTEXT}
confluenceSearch	how is find to {do something|RTEXT}
confluenceSearch	where is find to {do something|RTEXT}
confluenceSearch	who to find to {do something|RTEXT}
confluenceSearch	what to find to {do something|RTEXT}
confluenceSearch	how to find to {do something|RTEXT}
confluenceSearch	where to find to {do something|RTEXT}
confluenceSearch	who do find to {do something|RTEXT}
confluenceSearch	what do find to {do something|RTEXT}
confluenceSearch	how do find to {do something|RTEXT}
confluenceSearch	where do find to {do something|RTEXT}
confluenceSearch	who I find to {do something|RTEXT}
confluenceSearch	what I find to {do something|RTEXT}
confluenceSearch	how I find to {do something|RTEXT}
confluenceSearch	where I find to {do something|RTEXT}
confluenceSearch	who search to {do something|RTEXT}
confluenceSearch	what search to {do something|RTEXT}
confluenceSearch	how search to {do something|RTEXT}
confluenceSearch	where search to {do something|RTEXT}
confluenceSearch	who is search to {do something|RTEXT}
confluenceSearch	what is search to {do something|RTEXT}
confluenceSearch	how is search to {do something|RTEXT}
confluenceSearch	where is search to {do something|RTEXT}
confluenceSearch	who to search to {do something|RTEXT}
confluenceSearch	what to search to {do something|RTEXT}
confluenceSearch	how to search to {do something|RTEXT}
confluenceSearch	where to search to {do something|RTEXT}
confluenceSearch	who do search to {do something|RTEXT}
confluenceSearch	what do search to {do something|RTEXT}
confluenceSearch	how do search to {do something|RTEXT}
confluenceSearch	where do search to {do something|RTEXT}
confluenceSearch	who I search to {do something|RTEXT}
confluenceSearch	what I search to {do something|RTEXT}
confluenceSearch	how I search to {do something|RTEXT}
confluenceSearch	where I search to {do something|RTEXT}
confluenceSearch	who talk to {do something|RTEXT}
confluenceSearch	what talk to {do something|RTEXT}
confluenceSearch	how talk to {do something|RTEXT}
confluenceSearch	where talk to {do something|RTEXT}
confluenceSearch	who is talk to {do something|RTEXT}
confluenceSearch	what is talk to {do something|RTEXT}
confluenceSearch	how is talk to {do something|RTEXT}
confluenceSearch	where is talk to {do something|RTEXT}
confluenceSearch	who to talk to {do something|RTEXT}
confluenceSearch	what to talk to {do something|RTEXT}
confluenceSearch	how to talk to {do something|RTEXT}
confluenceSearch	where to talk to {do something|RTEXT}
confluenceSearch	who do talk to {do something|RTEXT}
confluenceSearch	what do talk to {do something|RTEXT}
confluenceSearch	how do talk to {do something|RTEXT}
confluenceSearch	where do talk to {do something|RTEXT}
confluenceSearch	who I talk to {do something|RTEXT}
confluenceSearch	what I talk to {do something|RTEXT}
confluenceSearch	how I talk to {do something|RTEXT}
confluenceSearch	where I talk to {do something|RTEXT}
confluenceSearch	who the {do something|RTEXT}
confluenceSearch	what the {do something|RTEXT}
confluenceSearch	how the {do something|RTEXT}
confluenceSearch	where the {do something|RTEXT}
confluenceSearch	who is the {do something|RTEXT}
confluenceSearch	what is the {do something|RTEXT}
confluenceSearch	how is the {do something|RTEXT}
confluenceSearch	where is the {do something|RTEXT}
confluenceSearch	who to the {do something|RTEXT}
confluenceSearch	what to the {do something|RTEXT}
confluenceSearch	how to the {do something|RTEXT}
confluenceSearch	where to the {do something|RTEXT}
confluenceSearch	who do the {do something|RTEXT}
confluenceSearch	what do the {do something|RTEXT}
confluenceSearch	how do the {do something|RTEXT}
confluenceSearch	where do the {do something|RTEXT}
confluenceSearch	who I the {do something|RTEXT}
confluenceSearch	what I the {do something|RTEXT}
confluenceSearch	how I the {do something|RTEXT}
confluenceSearch	where I the {do something|RTEXT}
confluenceSearch	who get the {do something|RTEXT}
confluenceSearch	what get the {do something|RTEXT}
confluenceSearch	how get the {do something|RTEXT}
confluenceSearch	where get the {do something|RTEXT}
confluenceSearch	who is get the {do something|RTEXT}
confluenceSearch	what is get the {do something|RTEXT}
confluenceSearch	how is get the {do something|RTEXT}
confluenceSearch	where is get the {do something|RTEXT}
confluenceSearch	who to get the {do something|RTEXT}
confluenceSearch	what to get the {do something|RTEXT}
confluenceSearch	how to get the {do something|RTEXT}
confluenceSearch	where to get the {do something|RTEXT}
confluenceSearch	who do get the {do something|RTEXT}
confluenceSearch	what do get the {do something|RTEXT}
confluenceSearch	how do get the {do something|RTEXT}
confluenceSearch	where do get the {do something|RTEXT}
confluenceSearch	who I get the {do something|RTEXT}
confluenceSearch	what I get the {do something|RTEXT}
confluenceSearch	how I get the {do something|RTEXT}
confluenceSearch	where I get the {do something|RTEXT}
confluenceSearch	who find the {do something|RTEXT}
confluenceSearch	what find the {do something|RTEXT}
confluenceSearch	how find the {do something|RTEXT}
confluenceSearch	where find the {do something|RTEXT}
confluenceSearch	who is find the {do something|RTEXT}
confluenceSearch	what is find the {do something|RTEXT}
confluenceSearch	how is find the {do something|RTEXT}
confluenceSearch	where is find the {do something|RTEXT}
confluenceSearch	who to find the {do something|RTEXT}
confluenceSearch	what to find the {do something|RTEXT}
confluenceSearch	how to find the {do something|RTEXT}
confluenceSearch	where to find the {do something|RTEXT}
confluenceSearch	who do find the {do something|RTEXT}
confluenceSearch	what do find the {do something|RTEXT}
confluenceSearch	how do find the {do something|RTEXT}
confluenceSearch	where do find the {do something|RTEXT}
confluenceSearch	who I find the {do something|RTEXT}
confluenceSearch	what I find the {do something|RTEXT}
confluenceSearch	how I find the {do something|RTEXT}
confluenceSearch	where I find the {do something|RTEXT}
confluenceSearch	who search the {do something|RTEXT}
confluenceSearch	what search the {do something|RTEXT}
confluenceSearch	how search the {do something|RTEXT}
confluenceSearch	where search the {do something|RTEXT}
confluenceSearch	who is search the {do something|RTEXT}
confluenceSearch	what is search the {do something|RTEXT}
confluenceSearch	how is search the {do something|RTEXT}
confluenceSearch	where is search the {do something|RTEXT}
confluenceSearch	who to search the {do something|RTEXT}
confluenceSearch	what to search the {do something|RTEXT}
confluenceSearch	how to search the {do something|RTEXT}
confluenceSearch	where to search the {do something|RTEXT}
confluenceSearch	who do search the {do something|RTEXT}
confluenceSearch	what do search the {do something|RTEXT}
confluenceSearch	how do search the {do something|RTEXT}
confluenceSearch	where do search the {do something|RTEXT}
confluenceSearch	who I search the {do something|RTEXT}
confluenceSearch	what I search the {do something|RTEXT}
confluenceSearch	how I search the {do something|RTEXT}
confluenceSearch	where I search the {do something|RTEXT}
confluenceSearch	who talk the {do something|RTEXT}
confluenceSearch	what talk the {do something|RTEXT}
confluenceSearch	how talk the {do something|RTEXT}
confluenceSearch	where talk the {do something|RTEXT}
confluenceSearch	who is talk the {do something|RTEXT}
confluenceSearch	what is talk the {do something|RTEXT}
confluenceSearch	how is talk the {do something|RTEXT}
confluenceSearch	where is talk the {do something|RTEXT}
confluenceSearch	who to talk the {do something|RTEXT}
confluenceSearch	what to talk the {do something|RTEXT}
confluenceSearch	how to talk the {do something|RTEXT}
confluenceSearch	where to talk the {do something|RTEXT}
confluenceSearch	who do talk the {do something|RTEXT}
confluenceSearch	what do talk the {do something|RTEXT}
confluenceSearch	how do talk the {do something|RTEXT}
confluenceSearch	where do talk the {do something|RTEXT}
confluenceSearch	who I talk the {do something|RTEXT}
confluenceSearch	what I talk the {do something|RTEXT}
confluenceSearch	how I talk the {do something|RTEXT}
confluenceSearch	where I talk the {do something|RTEXT}
searchDetails	{UInput}
searchDetails	Number {UInput}
```

### Testing the skills
Use Sample Utterance to test the different intents.

#####Example Tests :
Enter Utterance :  How to connect to guest wifi ?

click on the play button in the Lamda respose to listen to the response

to Choose an option from the list, enter the following information

Enter Utterance : Number one

press the play button again to listen to response


To test the functionality on Alexa Echo  **Make sure to log in to Echo with the same account used to create the Alexa Function in the Amazon developer console. Only then will Alexa be able to recognize the Invocation Name for the skill.**


##### Other Information

Publishing information and privacy information are not required for setup the skill. They are need only when submitting the alexa skill for certification.


#### Enabling Confluence Search skill

To enable skill in echo , open the alexa app in your mobile and choose skills option from the menu.  Click on `Your Skills` on the top right corner of the screen.

The new skill that we created will pop up. Click on `Enable Skill`


#### References

[Follow the "Setting up AWS Lambda" instructions from the following link](https://www.bignerdranch.com/blog/developing-alexa-skills-locally-with-nodejs-deploying-your-skill-to-staging/)

[Follow the "Configuring the Skill Interface" instructions from the following link](https://www.bignerdranch.com/blog/developing-alexa-skills-locally-with-nodejs-deploying-your-skill-to-staging/)