'use strict';
var _ = require('lodash');
var rp = require('request-promise');
var url = require("url");
var config = require('./config');

function confluenceEndHelper() {};

var ENDPOINTURL = '/rest/api/content/search';
var ENDPOINTCONTENTURL = '/rest/api/content/';
var queryUrl = 'https://vxx4hyrn6l.execute-api.us-east-1.amazonaws.com/dev/getUrl/';
var expandview = '?expand=body.view';
var confluenceURL = config.ConfluenceBaseURL;
var confluenceSpace = config.ConfluenceSpace;

//GET call to Confluence to resolve issue
confluenceEndHelper.prototype.GetConfluenceURL = function() {
    if( (typeof confluenceURL == 'undefined') || (typeof confluenceSpace == 'undefined') ){
        return "Confluence U r l and space is not defined";
    }else{
        return "Confluence URL is "+ confluenceURL + " and space key associated is "+ confluenceSpace;
    }
};

//GET call to Confluence to search for matches
confluenceEndHelper.prototype.GetConfluence = function(searchPhrase) {
    console.log("the search pharse is " + searchPhrase);
    var options = {
        method: 'GET',
        uri : confluenceURL + ENDPOINTURL,
        qs : {
            limit : 5,
            cql : 'type = page and space = ' + confluenceSpace +' and siteSearch ~ "' + searchPhrase + '"'
        },
        json: true
    };
    return rp(options);
};

//GET call to Confluence to get content
confluenceEndHelper.prototype.GetConfluencePageContent = function(contentid) {
    var options = {
        method: 'GET',
        uri : confluenceURL + ENDPOINTCONTENTURL + contentid + expandview,
        json: true
    };
    return rp(options);
};

// Format confluence search
confluenceEndHelper.prototype.formatConfluenceStatus = function(results) {
    if (results.length === 0) {
        return 'No content found assciated to search Text';
    }

    var content = ' I found the following results from wiki <break time="500ms"/>  Please choose from the following <break time="800ms"/>';
    content += results.map(function(result,index) {
      return  eval(index + 1) + ' <break time="500ms"/> ' + result.title + ' <break time="500ms"/> ';
    });

    return content;
};

confluenceEndHelper.prototype.formatConfluencePageContent = function(response) {
    if ( typeof response.id === undefined ) {
        return 'No content found assciated to search Text';
    }

    var content = response.body.view.value;

    //remove all closing tags in html
    content = content.replace(/<\/[^>]*>/g,"");

    // replace all html tags as <p> tags
    content = content.replace(/<(?!\/)[^>]*>/g,"<p>");

    // replace multiple occurance of <p> tags with single <p> tag
    content = content.replace(/(<p>\s*)+/g,"<p>");

    // replace <p> tags with break pauses
    content = content.replace(/<(?!\/)[^>]*>/g,'<break time="500ms"/>');

    return content;
}



module.exports = confluenceEndHelper;
